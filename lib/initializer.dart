import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suitcore_andiez_onboarding/firebase_options.dart';
import 'package:suitcore_andiez_onboarding/model/event.dart';
import 'package:suitcore_andiez_onboarding/model/guest.dart';
import 'package:suitcore_andiez_onboarding/model/place.dart';
import 'data/local/hive/hive_adapters.dart';
import 'data/local/hive/hive_constants.dart';
import 'data/local/storage/storage_constants.dart';
import 'feature/auth/auth_controller.dart';
import 'model/user.dart';

class Initializer {
  static Future<void> init() async {
    try {
      await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      );

      OneSignal.shared.setAppId("db78028b-da40-46af-af66-b682a4b3359e");
      _initOneSignal();

      _initScreenPreference();
      await globalLocalData();
      globalController();
    } catch (err) {
      rethrow;
    }
  }

  static globalController() {
    Get.put<AuthController>(AuthController());
  }

  static globalLocalData() async {
    // Global Local Database
    await Get.putAsync<SharedPreferences>(() async {
      return await SharedPreferences.getInstance();
    });
    await Get.putAsync<FlutterSecureStorage>(() async {
      return FlutterSecureStorage();
    });
    await Get.putAsync<GetStorage>(() async {
      return GetStorage(StorageName.STORAGE_NAME);
    });
  }

  static Future<void> initHive() async {
    Directory dir = await getApplicationDocumentsDirectory();
    await Hive.initFlutter(dir.path);
    HiveAdapters().registerAdapter();
    await Hive.openBox<UserModel>(HiveConstants.USERS_BOX);
    await Hive.openBox<Place>(HiveConstants.PLACES);
    await Hive.openBox<Event>(HiveConstants.EVENTS);
    await Hive.openBox<Guest>(HiveConstants.GUESTS);
  }

  static void _initScreenPreference() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  static void _initOneSignal() {
    OneSignal.shared.setNotificationWillShowInForegroundHandler(
        (OSNotificationReceivedEvent event) {
      // Will be called whenever a notification is received in foreground
      // Display Notification, pass null param for not displaying the notification
      event.complete(event.notification);
    });

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      // Will be called whenever a notification is opened/button pressed.
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      // Will be called whenever the permission changes
      // (ie. user taps Allow on the permission prompt in iOS)
    });

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      // Will be called whenever the subscription changes
      // (ie. user gets registered with OneSignal and gets a user ID)
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges emailChanges) {
      // Will be called whenever then user's email subscription changes
      // (ie. OneSignal.setEmail(email) is called and the user gets registered
    });
  }
}
