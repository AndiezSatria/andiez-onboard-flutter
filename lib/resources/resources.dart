import 'package:flutter/material.dart';
import 'package:suitcore_andiez_onboarding/model/event.dart';

class Resources {
  static AppColors color = AppColors();
  static AppImages images = AppImages();
  static AppDummyData dummy = AppDummyData();
}

class AppColors {
  Color colorPrimary = Colors.deepOrange;
  Color colorSecondary = Colors.deepOrangeAccent;
  Color colorAccent = Colors.white;
  Color black = Colors.black;
  Color white = Colors.white;
  Color grey = Colors.grey;
  Color red = Colors.red;
  Color borderColor = Colors.black12;
  Color subHintColor = Colors.black45;
}

class AppImages {
  AssetImage imageLogo =
      AssetImage('lib/resources/images/ic_logo_suitcore_main.png');
  AssetImage imageEmpty = AssetImage('lib/resources/images/img_empty.png');
  AssetImage imageError = AssetImage('lib/resources/images/img_error.png');
  AssetImage userPlaceholder =
      AssetImage('lib/resources/images/user_placeholder.png');
  AssetImage bgValidation =
      AssetImage('lib/resources/images/bg_validation.png');
  AssetImage btnAddPhoto =
      AssetImage('lib/resources/images/btn_add_photo.png');
}

class AppDummyData {
  List<Event> events = [
    Event(
      id: 1,
      name: "Event ABC",
      imageUrl:
          "https://media-cdn.tripadvisor.com/media/photo-s/09/5a/84/aa/amazon-forest-day-tours.jpg",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor pulvinar suscipit. Morbi a blandit ante. Sed in elit varius odio sodales maximus non quis eros. Sed urna nibh, consectetur vel neque congue, pharetra gravida augue. Proin a pretium ipsum. Proin pharetra efficitur tempus. Ut dignissim porta vestibulum. Sed eu pretium augue. Praesent volutpat enim sit amet quam auctor, nec efficitur massa congue. Pellentesque semper interdum ultricies. Aenean enim urna, cursus a urna eget, vestibulum varius lorem. Aliquam sit amet neque interdum ante consequat eleifend. Sed ut quam eros. Vestibulum ultrices semper tellus vitae hendrerit. Curabitur dapibus ligula odio, nec venenatis mauris pharetra a.",
      date: "20-10-2021",
      tags: ["#Bussiness", "#Marathon", "#Triathon"],
      lat: -6.970208883099997,
      lon: 107.63015541209909,
    ),
    Event(
      id: 2,
      name: "Event Running",
      imageUrl:
          "https://media-cdn.tripadvisor.com/media/photo-s/09/5a/84/aa/amazon-forest-day-tours.jpg",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor pulvinar suscipit. Morbi a blandit ante. Sed in elit varius odio sodales maximus non quis eros. Sed urna nibh, consectetur vel neque congue, pharetra gravida augue. Proin a pretium ipsum. Proin pharetra efficitur tempus. Ut dignissim porta vestibulum. Sed eu pretium augue. Praesent volutpat enim sit amet quam auctor, nec efficitur massa congue. Pellentesque semper interdum ultricies. Aenean enim urna, cursus a urna eget, vestibulum varius lorem. Aliquam sit amet neque interdum ante consequat eleifend. Sed ut quam eros. Vestibulum ultrices semper tellus vitae hendrerit. Curabitur dapibus ligula odio, nec venenatis mauris pharetra a.",
      date: "12-05-2020",
      tags: ["#Bussiness", "#Marathon", "#Triathon"],
      lat: -6.901964000723139,
      lon: 107.6181265900032,
    ),
    Event(
      id: 3,
      name: "Event Marathon",
      imageUrl:
          "https://media-cdn.tripadvisor.com/media/photo-s/09/5a/84/aa/amazon-forest-day-tours.jpg",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor pulvinar suscipit. Morbi a blandit ante. Sed in elit varius odio sodales maximus non quis eros. Sed urna nibh, consectetur vel neque congue, pharetra gravida augue. Proin a pretium ipsum. Proin pharetra efficitur tempus. Ut dignissim porta vestibulum. Sed eu pretium augue. Praesent volutpat enim sit amet quam auctor, nec efficitur massa congue. Pellentesque semper interdum ultricies. Aenean enim urna, cursus a urna eget, vestibulum varius lorem. Aliquam sit amet neque interdum ante consequat eleifend. Sed ut quam eros. Vestibulum ultrices semper tellus vitae hendrerit. Curabitur dapibus ligula odio, nec venenatis mauris pharetra a.",
      date: "12-05-2022",
      tags: ["#Bussiness", "#Marathon", "#Triathon"],
      lat: -6.919948231142309,
      lon: 107.60686733426512,
    ),
    Event(
      id: 4,
      name: "Event Relay Run",
      imageUrl:
          "https://media-cdn.tripadvisor.com/media/photo-s/09/5a/84/aa/amazon-forest-day-tours.jpg",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor pulvinar suscipit. Morbi a blandit ante. Sed in elit varius odio sodales maximus non quis eros. Sed urna nibh, consectetur vel neque congue, pharetra gravida augue. Proin a pretium ipsum. Proin pharetra efficitur tempus. Ut dignissim porta vestibulum. Sed eu pretium augue. Praesent volutpat enim sit amet quam auctor, nec efficitur massa congue. Pellentesque semper interdum ultricies. Aenean enim urna, cursus a urna eget, vestibulum varius lorem. Aliquam sit amet neque interdum ante consequat eleifend. Sed ut quam eros. Vestibulum ultrices semper tellus vitae hendrerit. Curabitur dapibus ligula odio, nec venenatis mauris pharetra a.",
      date: "12-05-2021",
      tags: ["#Bussiness", "#Marathon", "#Triathon"],
      lat: -6.920933403629006,
      lon: 107.60951975479529,
    ),
  ];
}
