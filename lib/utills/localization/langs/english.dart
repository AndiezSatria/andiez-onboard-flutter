const Map<String, String> en = {
  'txt_name': 'name',
  'txt_email': 'Email',
  'txt_password': 'Password',
  'txt_menu_home': 'Home', // with argument using %s
  'txt_menu_dialog': 'Dialogs',
  'txt_menu_maps': 'Maps',
  'txt_menu_other': 'Others',
  'txt_menu_login': 'Login',
  'txt_menu_detail': 'Detail',
  'txt_empty_user': 'Data users not found',
  'txt_error_title': 'Something when wrong',
  'txt_error_general': 'Oops something when wrong...',
  'txt_input_name': 'Masukkan Nama',
  'txt_title_home': "Home",
  'txt_title_name': "Name :",
  'txt_button_event': "Select Event",
  'txt_button_guest': "Select Guest",
  'txt_button_notification': "Send Notification",
  'txt_button_fetch': "Connect",

  //BUTTON
  'txt_button_retry': 'Retry',
  'txt_button_change_locale': 'Change Language',
  'txt_button_login': 'Login',
  'txt_button_logout': 'Log out',
  'txt_button_camera': 'Camera',
  'txt_button_gallery': 'Gallery',

  //Validation
  'txt_valid_email': 'Please enter a valid email address.',
  'txt_valid_password': 'Password must be at least 6 characters.',
  'txt_valid_name': 'Please enter a name.',
  'txt_valid_number': 'Please enter a number.',
  'txt_valid_notEmpty': 'This is a required field.',
};
