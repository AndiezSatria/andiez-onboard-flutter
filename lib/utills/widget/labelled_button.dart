import 'package:flutter/material.dart';

class LabelledButton extends StatelessWidget {
  const LabelledButton({
    Key? key,
    this.textColor = Colors.white,
    this.borderColor = Colors.white,
    required this.label,
    required this.onPressed,
  }) : super(key: key);
  final Color textColor;
  final Color borderColor;
  final String label;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onPressed();
      },
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: borderColor,
          ),
        ),
        child: Center(
          child: Text(
            label,
            style:
                Theme.of(context).textTheme.button?.copyWith(color: textColor),
          ),
        ),
      ),
    );
  }
}
