import 'package:hive/hive.dart';
import 'package:suitcore_andiez_onboarding/data/local/hive/hive_types.dart';

part 'guest.g.dart';

@HiveType(typeId: HiveTypes.guest)
class Guest extends HiveObject {
  Guest({
    required this.id,
    this.name,
    this.birthDate,
    this.image,
  });

  @HiveField(0)
  int id;
  @HiveField(1)
  String? name;
  @HiveField(2)
  String? birthDate;
  @HiveField(3)
  String? image;

  factory Guest.fromJson(Map<String, dynamic> json) => Guest(
        id: json['id'],
        name: json['name'],
        birthDate: json['birthdate'],
        image:
            "https://media-cdn.tripadvisor.com/media/photo-i/1a/9e/40/c0/alas-purwo-is-one-of.jpg",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "birthDate": birthDate,
        "image": image,
      };
}
