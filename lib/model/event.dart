import 'package:hive/hive.dart';
import 'package:suitcore_andiez_onboarding/data/local/hive/hive_types.dart';
import 'package:suitcore_andiez_onboarding/data/remote/wrapper/model_factory.dart';

part 'event.g.dart';


@HiveType(typeId: HiveTypes.event)
class Event extends HiveObject implements ModelFactory {
  Event({
    required this.id,
    this.name,
    this.imageUrl,
    this.description,
    this.date,
    this.tags,
    this.lat,
    this.lon,
  });

	@HiveField(0)
  int id;
	@HiveField(1)
  String? name;
	@HiveField(2)
  String? imageUrl;
	@HiveField(3)
  String? description;
	@HiveField(4)
  String? date;
	@HiveField(5)
  List<String>? tags;
	@HiveField(6)
  double? lat;
	@HiveField(7)
  double? lon;

  factory Event.fromJson(Map<String, dynamic> json) => Event(
        id: json["id"],
        name: json["name"],
        imageUrl: json["imageUrl"],
        description: json["description"],
        date: json["date"],
        tags: json["tags"],
        lat: json['lat'],
        lon: json['lon'],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "imageUrl": imageUrl,
        "description": description,
        "date": date,
        "tags": tags,
        "lat": lat,
        "lon": lon,
      };
}