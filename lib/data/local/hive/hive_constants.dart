class HiveConstants {
  static const USERS_BOX = "users";
  static const PLACES = "places";
  static const EVENTS = "events";
  static const GUESTS = "guests";
}
