import 'package:hive/hive.dart';
import 'package:suitcore_andiez_onboarding/model/event.dart';
import 'package:suitcore_andiez_onboarding/model/guest.dart';
import 'package:suitcore_andiez_onboarding/model/place.dart';
import 'package:suitcore_andiez_onboarding/model/user.dart';

class HiveAdapters {
  void registerAdapter() {
    Hive.registerAdapter<UserModel>(UserAdapter());
    Hive.registerAdapter<Place>(PlaceAdapter());
    Hive.registerAdapter<Event>(EventAdapter());
    Hive.registerAdapter<Guest>(GuestAdapter());
  }
}
