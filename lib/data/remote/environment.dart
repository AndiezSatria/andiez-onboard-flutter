class Environments {
  static const String PRODUCTION = 'https://www.mocky.io/v2';
  static const String DEV = 'https://www.mocky.io/v2';
}

class ConfigEnvironments {
  static const String _currentEnvironments = Environments.DEV;
  static const List<Map<String, String>> _availableEnvironments = [
    {
      'env': Environments.DEV,
      'url': 'https://www.mocky.io/v2/',
    },
    {
      'env': Environments.PRODUCTION,
      'url': 'https://www.mocky.io/v2/',
    },
  ];

  static String? getEnvironments() {
    return _availableEnvironments
        .firstWhere(
          (d) => d['env'] == _currentEnvironments,
        )
        .values
        .last
        .toString();
  }
}
