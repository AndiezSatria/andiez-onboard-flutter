import 'package:suitcore_andiez_onboarding/model/login_result.dart';
import 'package:suitcore_andiez_onboarding/model/place.dart';
import 'package:suitcore_andiez_onboarding/model/user.dart';

abstract class ModelFactory {
  static ModelFactory fromJson<T>(Map<String, dynamic> json) {
    if (typeEqualn<T, UserModel>()) {
      return UserModel.fromJson(json);
    } else if (typeEqualn<T, Place>()) {
      return Place.fromJson(json);
    } else if (typeEqualn<T, LoginResult>()) {
      return LoginResult.fromJson(json);
    }

    throw UnimplementedError('`$T` fromJson factory unimplemented.');
  }

  static Map<String, dynamic> toJson<T>(T obj) {
    if (typeEqualn<T, UserModel>()) {
      return (obj as UserModel).toJson();
    } else if (typeEqualn<T, Place>()) {
      return (obj as Place).toJson();
    } else if (typeEqualn<T, LoginResult>()) {
      return (obj as LoginResult).toJson();
    }

    throw UnimplementedError('`$T` toJson factory unimplemented.');
  }
}

/// Cek kesamaan tipe. Nullable dianggap beda.
/// `Model` == `Model`, `Model` != `Model?`.
bool typeEqual<S, T>() => S == T;

/// Cek kesamaan tipe. Nullable dianggap sama.
/// `Model` == `Model`, `Model` == `Model?`.
bool typeEqualn<S, T>() {
  return typeEqual<S, T>() || typeEqual<S?, T?>();
}
