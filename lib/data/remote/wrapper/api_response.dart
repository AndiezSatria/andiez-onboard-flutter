import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';
import 'model_factory.dart';

class ApiResponse<T> extends BaseResponse {
  final T? result;

  ApiResponse({
    required int status,
    required String message,
    required String domain,
    this.result,
  }) : super(status: status, message: message, domain: domain);

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse<T>(
      status: json["status"] as int,
      message: json["message"] as String,
      domain: json["domain"] as String,
      result: json["result"] == null
          ? null
          : _Converter<T>().fromJson(json["result"]),
    );
  }

  Map<String, dynamic> toJson() => {
        'status': this.status,
        'message': this.message,
        'domain': this.domain,
        'result':
            this.result == null ? null : _Converter<T>().toJson(this.result!),
      };
}

class ApiResponses<T> extends BaseResponse {
  List<T>? data;

  ApiResponses({
    required int status,
    required String message,
    required String domain,
    this.data = const [],
  }) : super(status: status, message: message, domain: domain);

  factory ApiResponses.fromJson(Map<String, dynamic> json) => ApiResponses<T>(
        status: json["status"],
        message: json["message"],
        domain: json["domain"],
        data: json["result"] == null
            ? []
            : List<T>.from(
                json["result"].map((x) => _Converter<T>().fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        'status': this.status,
        'message': this.message,
        'domain': this.domain,
        'result': this.data?.map(_Converter<T>().toJson).toList(),
      };
}

class _Converter<T> implements JsonConverter<T, Object> {
  const _Converter();

  @override
  T fromJson(Object json) {
    if (json is Map<String, dynamic>) {
      return ModelFactory.fromJson<T>(json) as T;
    }
    return json as T;
  }

  @override
  Map<String, dynamic> toJson(T object) {
    if (object is ModelFactory) {
      return ModelFactory.toJson<T>(object);
    }
    return {};
  }
}
