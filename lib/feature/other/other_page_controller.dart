import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/auth/auth_controller.dart';
import 'package:suitcore_andiez_onboarding/model/user.dart';

class OtherController extends GetxController {
  final AuthController authController = AuthController.find;

  UserModel? get user => authController.user;

  Future<void> signOut() async {
    await authController.signOut();
  }
}
