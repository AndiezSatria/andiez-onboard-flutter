import 'package:flutter/material.dart';
import 'package:suitcore_andiez_onboarding/data/remote/api_services.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_object_controller.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_refresher_status.dart';
import 'package:suitcore_andiez_onboarding/feature/auth/auth_controller.dart';
import 'package:suitcore_andiez_onboarding/model/user.dart';
import 'package:suitcore_andiez_onboarding/data/remote/errorhandler/error_handler.dart';

class LoginController extends BaseObjectController<UserModel> {
  final AuthController authController = AuthController.find;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void onClose() {
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }

  void signInWithEmailAndPassword() async {
    loadingState();
    await client().then((value) => value
            .login(emailController.text, passwordController.text)
            .validateStatus()
            .then((value) {
          authController.saveAuthData(
            user: value.result!.user!,
            token: value.result!.token!,
          );
          finishLoadData();
          authController.setAuth();
        }).handleError((onError) {
          finishLoadData(errorMessage: onError);
        }));
  }

  void bypassLogin() async {
    status = RefresherStatus.loading;
    update();
    await Future.delayed(Duration(seconds: 2));
    var user = UserModel(
        id: 1,
        name: "suitmedian",
        email: emailController.text,
        gender: "none",
        status: "admin");
    authController.saveAuthData(user: user, token: "a");
    finishLoadData();
    authController.setAuth();
  }
}
