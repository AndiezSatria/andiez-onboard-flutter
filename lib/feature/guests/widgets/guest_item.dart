import 'package:flutter/material.dart';
import 'package:suitcore_andiez_onboarding/model/guest.dart';

class GuestItem extends StatelessWidget {
  final Guest mData;
  final Function onPressed;
  const GuestItem({
    Key? key,
    required this.mData,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {
          onPressed();
        },
        splashColor: Colors.orangeAccent,
        child: Column(
          children: [
            Image.network(
              mData.image!,
              fit: BoxFit.cover,
              width: MediaQuery.of(context).size.width,
              height: 150,
            ),
            Text(
              mData.name ?? "",
              style: Theme.of(context).textTheme.bodyText1,
            ),
            Text(
              mData.birthDate ?? "",
              style: Theme.of(context).textTheme.caption,
            ),
          ],
        ),
      ),
    );
  }
}
