import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:suitcore_andiez_onboarding/feature/guests/guest_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/guests/widgets/guest_item.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/label_button.dart';

class GuestListBuilder extends StatelessWidget {
  final GuestController controller;
  const GuestListBuilder({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: false,
      controller: controller.refreshController,
      onRefresh: controller.getGuests,
      onLoading: controller.loadNextPage,
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: controller.dataList.length,
        itemBuilder: (context, index) {
          return GuestItem(
            mData: controller.dataList[index],
            onPressed: () {
              List<String> date =
                  controller.dataList[index].birthDate?.split("-") ?? [];
              int number = int.parse(date[2]);
              String text = "";
              if (number % 2 == 0 && number % 3 == 0) {
                text += "iOS";
              } else if (number % 2 == 0) {
                text += "blackberry";
              } else if (number % 3 == 0) {
                text += "android";
              } else {
                text += "feature phone";
              }

              if (controller.isPrime(number)) {
                text += " isPrime";
              } else {
                text += " not Prime";
              }
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Is Palindrome"),
                    content: Text(text),
                    actions: [
                      LabelButton(
                        labelText: "OK",
                        onPressed: () {
                          Get.back();
                          controller.homeController
                              .setGuest(controller.dataList[index].name ?? "");
                        },
                      ),
                    ],
                  );
                },
              );
            },
          );
        },
      ),
    );
  }
}
