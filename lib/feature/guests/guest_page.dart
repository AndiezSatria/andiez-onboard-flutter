import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/guests/guest_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/guests/widgets/guest_list_builder.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/sm_app_bar.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/state_handle_widget.dart';

class GuestPage extends StatelessWidget {
  const GuestPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SMAppBar.primaryAppbar(titleString: "Guest"),
      body: GetBuilder<GuestController>(
        builder: (controller) => StateHandleWidget(
          loadingEnabled: controller.isShimmering,
          emptyEnabled: controller.isEmptyData,
          emptySubtitle: 'txt_empty_user'.tr,
          errorEnabled: controller.isError,
          errorTitle: 'txt_error_general'.tr,
          onRetryPressed: () {
            controller.refreshPage();
          },
          body: GuestListBuilder(controller: controller),
        ),
      ),
    );
  }
}
