import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/data/local/hive/hive_constants.dart';
import 'package:suitcore_andiez_onboarding/data/remote/api_services.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_list_controller.dart';
import 'package:suitcore_andiez_onboarding/data/remote/errorhandler/error_handler.dart';
import 'package:suitcore_andiez_onboarding/feature/home/home_controller.dart';
import 'package:suitcore_andiez_onboarding/model/guest.dart';

class GuestController extends BaseListController<Guest> {
  final HomeController homeController = Get.find<HomeController>();

  @override
  void onInit() {
    getCacheBox(HiveConstants.GUESTS).then((value) => getGuests());
    super.onInit();
  }

  @override
  void loadNextPage() {
    // getGuests();
  }

  @override
  void refreshPage() {
    getGuests();
  }

  Future<void> getGuests() async {
    loadingState();
    await client().then(
      (value) => value.getGuests().then((data) {
        hasNext = data?.isNotEmpty ?? false;
        this.page = page;
        dataList.clear();
        saveCacheAndFinish(data, idList: data?.map((e) => e.id.toString()));
        finishLoadData(list: data != null ? data : []);
      }).handleError((onError) {
        finishLoadData(errorMessage: onError.toString());
        debugPrint("error : " + onError.toString());
      }),
    );
  }

  bool isPrime(int number) {
    if (number <= 3) return number > 1;
    if (number % 2 == 0 || number % 3 == 0) return false;
    var i = 5;
    while (i * i <= number) {
      if (number % i == 0 || number % (i + 2) == 0) return false;
      i += 6;
    }
    return true;
  }
}
