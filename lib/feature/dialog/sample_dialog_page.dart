import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/gen/assets.gen.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/base_empty_page.dart';

class DialogPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('txt_menu_dialog'.tr),
      ),
      body: EmptyPage(
        image: Assets.lib.resources.images.emptyStateCode,
        message: "No dialog added yet",
        buttonText: "Refresh",
        onPressed: () {},
      ),
    );
  }
}
