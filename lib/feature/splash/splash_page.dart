import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:suitcore_andiez_onboarding/feature/splash/splash_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/splash/splash_state.dart';
import 'package:suitcore_andiez_onboarding/resources/resources.dart';
import 'package:suitcore_andiez_onboarding/routes/page_names.dart';
import 'package:suitcore_andiez_onboarding/utills/helper/validator.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/forms/form_input_field_with_icon.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/label_button.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/labelled_button.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/statefull_wrapper.dart';

class SplashPage extends StatelessWidget {
  SplashPage({Key? key}) : super(key: key);
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      onInit: () async {
        var storage = await Permission.storage.status;
        if (storage.isDenied) {
          await Permission.storage.request();
        }
      },
      child: Scaffold(
        body: GetBuilder<SplashController>(
          builder: (controller) => Stack(
            children: [
              Stack(
                children: [
                  Image(
                    image: Resources.images.bgValidation,
                    fit: BoxFit.fill,
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                  ),
                  SafeArea(
                    child: SingleChildScrollView(
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              "Selamat Datang!",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  ?.copyWith(color: Colors.white),
                            ),
                            SizedBox(
                              width: 250,
                              child: Text(
                                "Sertakan gambar profil Anda untuk melengkapi profil.",
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(color: Colors.white),
                              ),
                            ),
                            Form(
                              key: _formKey,
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  children: [
                                    Container(
                                      width: 150,
                                      height: 150,
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 20),
                                      child: Image(
                                        image: Resources.images.btnAddPhoto,
                                      ),
                                    ),
                                    SizedBox(height: 16.0),
                                    FormInputFieldWithIcon(
                                      enabled: !controller.isLoading,
                                      controller: controller.nameController,
                                      iconPrefix: Icons.lock,
                                      labelText: 'txt_input_name'.tr,
                                      validator: Validator().name,
                                      onChanged: (value) => null,
                                      onSaved: (value) => controller
                                          .nameController.text = value!,
                                      maxLines: 1,
                                    ),
                                    SizedBox(height: 16.0),
                                    LabelledButton(
                                      label: "SELESAI",
                                      onPressed: () {
                                        if (_formKey.currentState!.validate()) {
                                          showDialog(
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: Text("Is Palindrome"),
                                                content: Text(controller
                                                        .isPalindrome(controller
                                                            .nameController
                                                            .text)
                                                    ? "Palindrome"
                                                    : "Not Palindrome"),
                                                actions: [
                                                  LabelButton(
                                                    labelText: "OK",
                                                    onPressed: () {
                                                      Get.back();
                                                      Get.toNamed(
                                                        PageName.HOME,
                                                        arguments: controller
                                                            .nameController
                                                            .text,
                                                      );
                                                    },
                                                  ),
                                                ],
                                              );
                                            },
                                          );
                                        }
                                      },
                                    ),
                                    SizedBox(height: 16.0),
                                    LabelledButton(
                                      label: controller.state?.userStatus ==
                                              UserType.UNAUTHENTICATED
                                          ? "Login"
                                          : "Logout",
                                      onPressed: () async {
                                        if (controller.state?.userStatus ==
                                            UserType.UNAUTHENTICATED) {
                                          try {
                                            await controller.signInwithGoogle();
                                          } catch (e) {}
                                        } else {
                                          await controller.signOutFromGoogle();
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
