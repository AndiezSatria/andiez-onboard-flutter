import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_object_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/splash/splash_state.dart';

class SplashController extends BaseObjectController<User> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  Rxn<SplashState> splashState = Rxn<SplashState>();

  Stream<SplashState?> get stream => splashState.stream;
  SplashState? get state => splashState.value;

  TextEditingController nameController = TextEditingController();

  @override
  void onInit() {
    splashState.value = SplashState(
        userStatus: _auth.currentUser == null
            ? UserType.UNAUTHENTICATED
            : UserType.AUTHENTICATED,
        user: _auth.currentUser);
    super.onInit();
  }

  @override
  void onReady() async {
    ever(splashState, authChanged);
    authChanged(state);
    super.onReady();
  }

  authChanged(SplashState? state) async {
    update();
  }

  @override
  void onClose() {
    nameController.dispose();
    super.onClose();
  }

  bool isPalindrome(String words) {
    String newWords = words.replaceAll(" ", "").toLowerCase();
    String reversed = newWords.characters.toList().reversed.join();

    return newWords == reversed;
  }

  Future<String?> signInwithGoogle() async {
    try {
      final GoogleSignInAccount? googleSignInAccount =
          await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount!.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );
      await _auth.signInWithCredential(credential);
      splashState.value = SplashState(userStatus: UserType.AUTHENTICATED);
    } on FirebaseAuthException catch (e) {
      print(e.message);
      throw e;
    }
  }

  Future<void> signOutFromGoogle() async {
    await _googleSignIn.signOut();
    await _auth.signOut();
    splashState.value = SplashState(userStatus: UserType.UNAUTHENTICATED);
  }
}
