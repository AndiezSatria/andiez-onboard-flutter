import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';

enum UserType { UNAUTHENTICATED, AUTHENTICATED }

class SplashState extends Equatable {
  final User? user;
  final UserType userStatus;

  const SplashState({this.user, this.userStatus = UserType.UNAUTHENTICATED});

  SplashState copyWith({
    User? user,
    UserType? userStatus,
  }) {
    return SplashState(
      user: user ?? this.user,
      userStatus: userStatus ?? this.userStatus,
    );
  }

  @override
  List<Object?> get props => [user, userStatus];
}
