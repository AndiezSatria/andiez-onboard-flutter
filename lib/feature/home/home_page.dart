import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/home/home_controller.dart';
import 'package:suitcore_andiez_onboarding/routes/page_names.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/primary_button.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/sm_app_bar.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SMAppBar.primaryAppbar(titleString: 'txt_title_home'.tr),
      body: GetBuilder<HomeController>(
        builder: (controller) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('txt_title_name'.tr),
                      Text(
                        controller.name,
                        textAlign: TextAlign.end,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              PrimaryButton(
                title: controller.selectedEvent.isEmpty
                    ? 'txt_button_event'.tr
                    : controller.selectedEvent,
                onPressed: () {
                  Get.toNamed(PageName.EVENT);
                },
              ),
              SizedBox(height: 16.0),
              PrimaryButton(
                title: controller.selectedGuest.isEmpty
                    ? 'txt_button_guest'.tr
                    : controller.selectedGuest,
                onPressed: () {
                  Get.toNamed(PageName.GUEST);
                },
              ),
              SizedBox(height: 16.0),
              PrimaryButton(
                title: 'txt_button_notification'.tr,
                onPressed: () {},
              ),
              SizedBox(height: 16.0),
              PrimaryButton(
                title: 'txt_button_fetch'.tr,
                onPressed: () {
                  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text()));
                },
              ),
              SizedBox(height: 16.0),
            ],
          ),
        ),
      ),
    );
  }
}
