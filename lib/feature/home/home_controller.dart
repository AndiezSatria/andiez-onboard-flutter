import 'package:get/get.dart';

class HomeController extends GetxController {
  HomeController({required this.name});
  late String selectedEvent;
  late String selectedGuest;
  String name;

  @override
  void onInit() {
    selectedEvent = "";
    selectedGuest = "";
    super.onInit();
  }

  void setEvent(String event) {
    selectedEvent = event;
    update();
    Get.back();
  }

  void setGuest(String guest) {
    selectedGuest = guest;
    update();
    Get.back();
  }
}
