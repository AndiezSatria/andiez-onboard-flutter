import 'package:flutter/cupertino.dart';
import 'package:suitcore_andiez_onboarding/data/local/hive/hive_constants.dart';
import 'package:suitcore_andiez_onboarding/data/remote/api_services.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_list_controller.dart';
import 'package:suitcore_andiez_onboarding/data/remote/errorhandler/error_handler.dart';
import 'package:suitcore_andiez_onboarding/model/place.dart';

class PlaceController extends BaseListController<Place> {
  @override
  void onInit() {
    getCacheBox(HiveConstants.PLACES).then((value) => getPlaces());
    super.onInit();
  }

  @override
  void refreshPage() {
    getPlaces();
  }

  @override
  void loadNextPage() {
    getPlaces(page: page + 1);
  }

  Future<void> getPlaces({int page = 1}) async {
    loadingState();
    await client().then(
      (value) => value.getPlaces(page, perPage).validateResponse().then((data) {
        hasNext = data.data!.isNotEmpty;
        this.page = page;
        if (page == 1) {
          dataList.clear();
          saveCacheAndFinish(data.data,
              idList: data.data?.map((e) => e.id.toString()));
        } else {
          finishLoadData(list: data.data != null ? data.data! : []);
        }
      }).handleError((onError) {
        finishLoadData(errorMessage: onError.toString());
        debugPrint("error : " + onError.toString());
      }),
    );
  }
}
