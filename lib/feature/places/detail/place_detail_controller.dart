import 'package:flutter/cupertino.dart';
import 'package:suitcore_andiez_onboarding/data/local/hive/hive_constants.dart';
import 'package:suitcore_andiez_onboarding/data/remote/api_services.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_object_controller.dart';
import 'package:suitcore_andiez_onboarding/data/remote/errorhandler/error_handler.dart';
import 'package:suitcore_andiez_onboarding/model/place.dart';

class PlaceDetailController extends BaseObjectController<Place> {
  PlaceDetailController({String? id}) : super(id: id);

  @override
  void onInit() {
    getCacheBox(HiveConstants.PLACES).then((value) => getPlaceDetail());
    super.onInit();
  }

  Future<void> getPlaceDetail() async {
    loadingState();
    await client().then(
      (value) =>
          value.getPlaceDetail(int.parse(id!)).validateResponse().then((data) {
        saveCacheBoxAndFinish(data.result, hiveConst: HiveConstants.PLACES);
      }).handleError((error) {
        finishLoadData(errorMessage: error.toString());
        debugPrint(error.toString());
      }),
    );
  }
}
