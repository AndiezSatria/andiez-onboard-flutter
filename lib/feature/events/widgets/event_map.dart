import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:suitcore_andiez_onboarding/feature/events/event_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/events/widgets/event_item.dart';
import 'package:suitcore_andiez_onboarding/utills/helper/map_helper.dart';

class EventMap extends StatelessWidget {
  final EventController controller;
  final String token =
      'pk.eyJ1IjoiYW5kaWV6cGVybWFuYSIsImEiOiJja3hnNzZ4OGUyYTNoMndwZnFxeXZjODdoIn0.zrQusZS1N8spOeTamPBAAA';
  final String style = 'mapbox://styles/mapbox/streets-v11';
  const EventMap({Key? key, required this.controller}) : super(key: key);

  void _onMapLongClickCallback(Point<double> point, LatLng coordinates) {}

  void _onCameraIdleCallback() {}

  void _onMapCreated(MapboxMapController controller) {
    controller.addListener(() {
      if (controller.isCameraMoving) {}
    });
    this.controller.setMapController(controller);
  }

  void _onStyleLoadedCallback() {
    print('onStyleLoadedCallback');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SmartRefresher(
            enablePullDown: true,
            enablePullUp: controller.hasNext,
            controller: controller.refreshController,
            onRefresh: controller.getEvents,
            onLoading: controller.loadNextPage,
            child: ListView.builder(
              itemCount: controller.dataList.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return EventItem(
                  mData: controller.dataList[index],
                  onPressed: () {
                    controller.homeController
                        .setEvent(controller.dataList[index].name ?? "");
                  },
                );
              },
            ),
          ),
        ),
        Expanded(
          child: MapboxMap(
            accessToken: token,
            styleString: style,
            initialCameraPosition: const CameraPosition(
              zoom: 15.0,
              target: LatLng(-6.908541493064672, 107.61808390697256),
            ),
            onMapCreated: (MapboxMapController controller) async {
              _onMapCreated(controller);
              final result = await acquireCurrentLocation();
              await controller.animateCamera(
                CameraUpdate.newLatLng(result!),
              );
            },
            trackCameraPosition: true,
            onMapLongClick: _onMapLongClickCallback,
            onCameraIdle: _onCameraIdleCallback,
            onStyleLoadedCallback: _onStyleLoadedCallback,
          ),
        ),
      ],
    );
  }
}
