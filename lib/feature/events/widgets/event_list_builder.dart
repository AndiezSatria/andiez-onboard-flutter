import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:suitcore_andiez_onboarding/feature/events/event_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/events/widgets/event_item.dart';

class EventListBuilder extends StatelessWidget {
  const EventListBuilder({Key? key, required this.controller})
      : super(key: key);
  final EventController controller;

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: controller.hasNext,
      controller: controller.refreshController,
      onRefresh: controller.getEvents,
      onLoading: controller.loadNextPage,
      child: ListView.builder(
        itemCount: controller.dataList.length,
        itemBuilder: (context, index) {
          return EventItem(
            mData: controller.dataList[index],
            onPressed: () {
              controller.homeController
                  .setEvent(controller.dataList[index].name ?? "");
            },
          );
        },
      ),
    );
  }
}
