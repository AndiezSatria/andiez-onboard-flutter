import 'package:get/get.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:suitcore_andiez_onboarding/data/remote/base/base_list_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/home/home_controller.dart';
import 'package:suitcore_andiez_onboarding/model/event.dart';
import 'package:suitcore_andiez_onboarding/resources/resources.dart';

class EventController extends BaseListController<Event> {
  final HomeController homeController = Get.find<HomeController>();
  late MapboxMapController mapController;

  @override
  void onInit() {
    getEvents();
    super.onInit();
  }

  @override
  void loadNextPage() {
    getEvents();
  }

  @override
  void refreshPage() {
    getEvents();
  }

  void setMapController(MapboxMapController controller) {
    mapController = controller;
  }

  Future<void> getEvents({int page = 1}) async {
    loadingState();
    await Future.delayed(Duration(seconds: 2));
    finishLoadData(list: Resources.dummy.events);
  }
}
