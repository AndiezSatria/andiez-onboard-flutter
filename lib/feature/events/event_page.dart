import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/events/event_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/events/widgets/event_list_builder.dart';
import 'package:suitcore_andiez_onboarding/resources/resources.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/sm_app_bar.dart';
import 'package:suitcore_andiez_onboarding/utills/widget/state_handle_widget.dart';

class EventPage extends StatelessWidget {
  const EventPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SMAppBar.primaryAppbar(
        titleString: "Event",
        actions: [
          IconButton(
            splashColor: Resources.color.colorPrimary,
            icon: const Icon(Icons.edit),
            onPressed: () {
              
            },
          ),
        ],
      ),
      body: GetBuilder<EventController>(
        builder: (controller) => StateHandleWidget(
          loadingEnabled: controller.isShimmering,
          emptyEnabled: controller.isEmptyData,
          emptySubtitle: 'txt_empty_user'.tr,
          errorEnabled: controller.isError,
          errorTitle: 'txt_error_general'.tr,
          onRetryPressed: () {
            controller.refreshPage();
          },
          body: EventListBuilder(controller: controller),
        ),
      ),
    );
  }
}
