import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:suitcore_andiez_onboarding/data/local/storage/storage_constants.dart';
import 'package:suitcore_andiez_onboarding/data/local/storage/storage_manager.dart';
import 'package:suitcore_andiez_onboarding/model/user.dart';
import 'package:suitcore_andiez_onboarding/routes/page_names.dart';
import 'package:suitcore_andiez_onboarding/utills/helper/secure_storage_manager.dart';
import 'package:suitcore_andiez_onboarding/utills/helper/shared_preference_manager.dart';

import 'auth_state.dart';

class AuthController extends GetxController {
  static AuthController find = Get.find();
  Rxn<AuthState> authState = Rxn<AuthState>();

  Stream<AuthState?> get stream => authState.stream;
  AuthState? get state => authState.value;

  var storage = StorageManager();
  var secureStorage = SecureStorageManager();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  UserModel? get user {
    if (storage.has(StorageName.USERS)) {
      return UserModel.fromJson(storage.get(StorageName.USERS));
    } else {
      return null;
    }
  }

  @override
  void onInit() {
    authState.value = AuthState(appStatus: AppType.INITIAL);
    super.onInit();
  }

  @override
  void onReady() async {
    ever(authState, authChanged);
    authChanged(state);
    super.onReady();
  }

  authChanged(AuthState? state) async {
    if (state?.appStatus == AppType.INITIAL) {
      await setup();
      checkUser();
    } else if (state?.appStatus == AppType.UNAUTHENTICATED) {
      clearData();
      Get.offAllNamed(PageName.SPLASH);
    } else if (state?.appStatus == AppType.AUTHENTICATED) {
      Get.offAllNamed(PageName.SPLASH);
    } else {
      Get.toNamed(PageName.LOADER);
    }
    update();
  }

  void checkUser() {
    if (storage.has(StorageName.USERS)) {
      setAuth();
    } else {
      signOut();
    }
  }

  void clearData() {
    if (storage.has(StorageName.USERS)) {
      storage.delete(StorageName.USERS);
    }
  }

  Future<void> saveAuthData({required UserModel user, required String token}) async {
    storage.write(StorageName.USERS, user.toJson());
    await secureStorage.setToken(value: token);
  }

  Future<void> signOut() async {
    await secureStorage.setToken(value: '');
    authState.value = AuthState(appStatus: AppType.UNAUTHENTICATED);
  }

  void setAuth() {
    authState.value = AuthState(appStatus: AppType.AUTHENTICATED);
  }

  setup() async {
    var _sp = SharedPreferenceManager();
    final firstInstall = _sp.getIsFirstInstall();
    if (firstInstall) {
      await secureStorage.setToken(value: '');
      _sp.setIsFirstInstall(value: false);
    }
  }

  Future<String?> signInwithGoogle() async {
    try {
      final GoogleSignInAccount? googleSignInAccount =
          await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount!.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );
      await _auth.signInWithCredential(credential);
      // splashState.value = SplashState(userStatus: UserType.AUTHENTICATED); 
      authState.value = AuthState(appStatus: AppType.AUTHENTICATED);
    } on FirebaseAuthException catch (e) {
      print(e.message);
      throw e;
    }
  }

  Future<void> signOutFromGoogle() async {
    await _googleSignIn.signOut();
    await _auth.signOut();
    authState.value = AuthState(appStatus: AppType.UNAUTHENTICATED);
    // splashState.value = SplashState(userStatus: UserType.UNAUTHENTICATED);
  }
}
