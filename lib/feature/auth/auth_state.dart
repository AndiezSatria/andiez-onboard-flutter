import 'package:equatable/equatable.dart';
import 'package:suitcore_andiez_onboarding/model/user.dart';

enum AppType { INITIAL, UNAUTHENTICATED, LOAD, AUTHENTICATED }

class AuthState extends Equatable {
  final UserModel? user;
  final AppType appStatus;

  const AuthState({this.user, this.appStatus = AppType.INITIAL});

  AuthState copyWith({
    UserModel? user,
    AppType? appStatus,
  }) {
    return AuthState(
      user: user ?? this.user,
      appStatus: appStatus ?? this.appStatus,
    );
  }

  @override
  List<Object?> get props => [user, appStatus];
}
