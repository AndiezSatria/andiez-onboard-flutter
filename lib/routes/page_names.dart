class PageName {
  static const DASHBOARD = "/dashboard";
  static const HOME = "/home";
  // static const DIALOG = "/dialog";
  static const LOGIN = "/login";
  static const MAPS = "/maps";
  static const OTHER = "/other";
  static const USER_DETAIL = "/user-detail";
  static const LOADER = "/loader";
  static const SPLASH = "/splash";
  static const EVENT = "/events";
  static const GUEST = "/guests";
}
