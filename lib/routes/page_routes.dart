import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/binding/dashboard_tab_binding.dart';
import 'package:suitcore_andiez_onboarding/binding/event_binding.dart';
import 'package:suitcore_andiez_onboarding/binding/guest_binding.dart';
import 'package:suitcore_andiez_onboarding/binding/home_binding.dart';
import 'package:suitcore_andiez_onboarding/binding/login_binding.dart';
import 'package:suitcore_andiez_onboarding/binding/place_detail_binding.dart';
import 'package:suitcore_andiez_onboarding/binding/splash_binding.dart';
import 'package:suitcore_andiez_onboarding/feature/dashboardtab/dashboard_tab_page.dart';
import 'package:suitcore_andiez_onboarding/feature/events/event_page.dart';
import 'package:suitcore_andiez_onboarding/feature/guests/guest_page.dart';
import 'package:suitcore_andiez_onboarding/feature/home/home_page.dart';
import 'package:suitcore_andiez_onboarding/feature/loader/loading_page.dart';
import 'package:suitcore_andiez_onboarding/feature/login/login_page.dart';
import 'package:suitcore_andiez_onboarding/feature/maps/maps_page.dart';
import 'package:suitcore_andiez_onboarding/feature/other/other_page.dart';
import 'package:suitcore_andiez_onboarding/feature/places/detail/place_detail_page.dart';
import 'package:suitcore_andiez_onboarding/feature/splash/splash_page.dart';

import 'page_names.dart';

class PageRoutes {
  static final pages = [
    GetPage(
      name: PageName.LOGIN,
      page: () => LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: PageName.LOADER,
      page: () => LoadingPage(),
    ),
    GetPage(
        name: PageName.DASHBOARD,
        page: () => DashBoardTabPage(),
        binding: DashBoardTabBinding()),
    GetPage(name: PageName.OTHER, page: () => OtherPage()),
    GetPage(
        name: PageName.USER_DETAIL,
        page: () => PlaceDetailPage(),
        binding: PlaceDetailBinding()),
    GetPage(
      name: PageName.SPLASH,
      page: () => SplashPage(),
      binding: SplashBindings(),
    ),
    GetPage(
      name: PageName.MAPS,
      page: () => MapsPage(),
      binding: SplashBindings(),
    ),
    GetPage(
      name: PageName.HOME,
      page: () => HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: PageName.EVENT,
      page: () => EventPage(),
      binding: EventBinding(),
    ),
    GetPage(
      name: PageName.GUEST,
      page: () => GuestPage(),
      binding: GuestBinding(),
    ),
  ];
}
