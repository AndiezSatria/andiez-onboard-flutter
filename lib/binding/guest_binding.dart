import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/guests/guest_controller.dart';

class GuestBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<GuestController>(GuestController());
  }
}
