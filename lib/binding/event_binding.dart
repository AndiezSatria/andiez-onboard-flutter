import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/events/event_controller.dart';

class EventBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<EventController>(EventController());
  }
}
