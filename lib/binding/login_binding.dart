import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/login/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<LoginController>(LoginController());
  }
}
