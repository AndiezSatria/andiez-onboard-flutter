import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/dashboardtab/dashboard_tab_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/dialog/dialog_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/maps/maps_controller.dart';
import 'package:suitcore_andiez_onboarding/feature/places/places_controller.dart';

class DashBoardTabBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashBoardTabController>(() => DashBoardTabController());
    Get.lazyPut<PlaceController>(() => PlaceController());
    Get.lazyPut<DialogController>(() => DialogController());
    Get.lazyPut<MapsController>(() => MapsController());
  }
}
