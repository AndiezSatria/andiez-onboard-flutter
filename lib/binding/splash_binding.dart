import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/splash/splash_controller.dart';

class SplashBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<SplashController>(SplashController());
  }
  
}