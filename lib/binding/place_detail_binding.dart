import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/places/detail/place_detail_controller.dart';

class PlaceDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlaceDetailController>(
        () => PlaceDetailController(id: Get.arguments));
  }
}
