import 'package:get/get.dart';
import 'package:suitcore_andiez_onboarding/feature/home/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController(name: Get.arguments));
  }
}
